import React, { Component } from "react";
import Select from 'react-select';
import DatePicker from "react-datepicker";
import { Chart } from "react-charts";
import Modal from 'react-responsive-modal';
import { BXDummy } from './bitrix_dummy.js';

import "react-datepicker/dist/react-datepicker.css";
import '../styles/app.css';
import '../styles/main.scss';


const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
  ];
  const data = [{ id: 1, title: 'Conan the Barbarian', year: '1981' },{ id: 2, title: 'Conan the Barbarian', year: '1982' },{ id: 3, title: 'Conan the Barbarian', year: '1983' },{ id: 4, title: 'Conan the Barbarian', year: '1984' }];
  const columns = [
    {
      name: 'Title',
      selector: 'title',
      sortable: true,
    },
    {
      name: 'Year',
      selector: 'year',
      sortable: true,
      right: true,
    },
  ];
  const lineChart = (
    // A react-chart hyper-responsively and continuusly fills the available
    // space of its parent element automatically
    <div
      style={{
        width: "400px",
        height: "300px"
      }}
    >
   <Chart
        series={{ type: 'area' }}
      data={[
        {
          label: "Series 1",
          data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]]
        },
        {
          label: "Series 2",
          data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4]]
        }
      ]}
      axes={[
        { primary: true, type: "linear", position: "bottom" },
        { type: "linear", position: "left" }
      ]}
      secondaryCursor
      primaryCursor
    />
    </div>
  );

class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            selectedOption: null,
            startDate: new Date(),
            open:false,
        }
        this.handleDataPickerChange = this.handleDataPickerChange.bind(this);
        this.openModel = this.onOpenModal.bind(this);
        this.closeModel = this.onCloseModal.bind(this);
    }

    componentWillMount(){
      let dummy = new BXDummy();
      dummy.CallBX("crm.deal.list",{ 
        ORDER: { "ID": "ASC" },
        FILTER: { ">ID": 5, "TITLE":"тест" },
        SELECT: [ "ID", "TITLE", "STAGE_ID", "PROBABILITY", "OPPORTUNITY", "CURRENCY_ID" ]
      }, this.showCheck.bind(this));
    }

    showCheck(res){
      console.log(res);
    }

    onOpenModal (){
        this.setState({ open: true });
    }

    onCloseModal(){
        this.setState({ open: false });
    }

    handleChange (selectedOption){
        let state = this.state;

        state.selectedOption = {...selectedOption};

        this.setState(state);
    }
    
    handleDataPickerChange(date){
        let state = this.state;

        console.log(date);
        state.startDate = date;

        this.setState(state);
    }

    render() {
        const { selectedOption } = this.state;
        return (
            <div>
                <div className="col-12">
                <Select
                    value={selectedOption}
                    onChange={this.handleChange.bind(this)}
                    options={options}
                    placeholder="Обрати"
                />
                </div>
                <div className="row  col-12">
                <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleDataPickerChange}
                    dateFormat="dd.MM.yyyy"
                />
                </div>
                { lineChart }
                <div>
                    <button onClick={this.onOpenModal.bind(this)}>Open modal</button>
                    <Modal open={this.state.open} onClose={this.onCloseModal.bind(this)} center>
                    <h2>Simple centered modal</h2>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default App;
import axios from 'axios';




const domain = "https://devos.bitrix24.ua";//ссылка на тестовый портал

/**
 * Класс для работы с данными битрикс на локал хосте, без заливания на 12 сервер
 *
 * @export
 * @class BXDummy
 */
export class BXDummy {
    constructor(){
        this.tempval = [];
        this.token = (localStorage.getItem("auth") == null ) ? "694dd95c003836960028e9de00000027000003d478f298f862f785a8d04684f19ac7a0" : localStorage.getItem("auth"); //"694dd95c003836960028e9de00000027000003d478f298f862f785a8d04684f19ac7a0"; //аксес токен приложения для работы с битриксом.
    }

    /**
     * Конструктор для http_build_query аналога php
     *
     * @param {object} formdata - обьект с даными что необходимо преобразовать.
     * @param {*} numeric_prefix - 
     * @param {*} arg_separator - 
     * @returns
     * @memberof BXDummy
    */
    http_build_query(formdata, numeric_prefix, arg_separator ){
        let key, use_val, use_key, i = 0, tmp_arr = [];

        if ( !arg_separator ){
            arg_separator = '&';
        }

        for (key in formdata){
            if ( typeof formdata[key] == 'object' ){
                for ( let inkey in formdata[key] ){
                    use_key = encodeURI(key);
                    use_val = encodeURI((formdata[key][inkey].toString()));
                    use_val = use_val.replace(/%20/g, '+');
                    
                    if(numeric_prefix && !isNaN(key)){
                        use_key = numeric_prefix + i;
                    }
                    tmp_arr[i] = use_key + '['+inkey+']=' + use_val;
                    i++;
                }
            }
            else{
                use_key = encodeURI(key);
                use_val = encodeURI((formdata[key].toString()));
                use_val = use_val.replace(/%20/g, '+');
                
                if(numeric_prefix && !isNaN(key)){
                    use_key = numeric_prefix + i;
                }
                tmp_arr[i] = use_key + '=' + use_val;
                i++;
            }
        }
        return tmp_arr.join(arg_separator);
    }

    /**
     * Выполняет роль заглушки битрикс 24, без необходимости каждый раз заливать код на 12 сервер
     * а просто пользоватся с локал хоста, при переносе обязательно заменить все функции на битрикс24 библиотеку
     *  
     * Для отлавливания ошибок, делайте проверку типа if ( res.error ) в error_discription будет текст ошибки от битрикса
     *
     *
     * @param {str} method - Какой метод хотим вызвать например crm.deal.list
     * @param {object} param - обьект параметров для битрикса
     * @param {function} callback - функция коллбэка в которую будут переданы результаты
     * @param {boolean} [more=false] - флаг при условие что больше 50 результатов и нужно перезапустить метод что бы забрать все
     * @param {number} [lastId=0] - id последнего элемента
     * @memberof BXDummy
    */
    CallBX(method, param, callback, more = false, lastId = 0) {


        if ( param.ORDER ){//В целях правильного забора данных если их больше 50, сортировка всегда будет по id от меньшего к большему
            param.ORDER = { "ID": "ASC" }
        }else{
            param["ORDER"] = { "ID": "ASC" }
        }
        if ( more ){//Если больше 50 результатов в фильтр добаляем доп условие.
            if ( param.FILTER ){
                param.FILTER[">ID"] = lastId;
            }
            else{
                param["FILTER"] = { ">ID":lastId }
            }
        }
        let formdata = this.http_build_query(param);//преобразовываем обьект с данными в http query
        axios.get(domain+"/rest/"+method+".json?"+"auth="+this.token+"&"+formdata)
        .then( (res)=>{

            this.tempval.push(res.data.result);
            if (res.data.total > 50 ){//Если результатов больше 50 делаем перевызов функции
                setTimeout(() => {
                    this.CallBX(domain, method, param, callback, true, res.data.result[res.data.result.length - 1]["ID"]);
                }, 500);
            }
            else {
                callback(this.tempval);
                this.tempval = [];
            }
        })
        .catch( (res)=>{
            if ( res.response ){
                if ( res.response.data.error = "expired_token" ){
                    //window.open('https://devos.bitrix24.ua/marketplace/app/75/', '_blank');
                    this.token = prompt("Введите новый auth приложения!");
                    localStorage.setItem("auth", this.token);
                    this.CallBX(method, param, callback);
                }
            }
            else{
                callback(res);
            }
        } )
    }
}
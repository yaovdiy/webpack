# Использовать только для одностраничных приложений.
---
### Используется следующие компоненты реакта:

* react-select - аналог select2 библиотеки для реакта.
 Документация [https://react-select.com/props]

* react-datepicker - аналог datepicker для реакта.
 Документация [https://reactdatepicker.com/]

* react-data-table-component - аналог для datatable для реакта.
 Документация [https://jbetancur.github.io/react-data-table-component/]

* react-charts - компонент для отрисовки графиков любой сложности
 Документация [https://www.npmjs.com/package/react-charts]

* react-responsive-modal - модальные окна для отображения ошибок или информации
 Документация [https://www.npmjs.com/package/react-responsive-modal]
---

### Подключеные модули
* axios - библиотека для работы с fetch запросами, которая использует promises
 Документация [https://www.npmjs.com/package/axios]

* bootstrap - css фрейморк для упрощения построения дизайна, для подключения используйте в основном css/scss файле команду 
```css
@import "~bootstrap/scss/bootstrap";
```
Для подключения bootstrap.js нужно в основном js файле указать 
```javascript
import 'bootstrap';
```
Документация [https://getbootstrap.com/docs/4.3/getting-started/introduction/]

* jquery - js библиотека для работы с dom структурой, используется для работы bootstrap 

* popper.js - js библиотека для работы с bootstrap

* react, react-dom - библиотека реакта.
 Документация  [https://ru.reactjs.org/docs/getting-started.html]

* redux, redux-thunk - библиотка для работы с редаксом.